#include "stm32f10x.h"
#include "stdlib.h"

// **********************      Definitions      ***********************
#define	BYTE	unsigned char		//u8
#define	WORD	unsigned short		//u16
#define	DWORD	unsigned long		//u32
	
//****************** Registers ********************
#define	R_DeviceType					0x00
#define	R_EventMask						0x01
#define	R_EventFlag						0x02
#define	R_FWVersion						0x03
#define	R_ApertureSize					0x04
#define	R_ApertureOrientation			0x05
#define	R_Calibration					0x06
#define	R_HeartOutFreq					0x07
#define	R_HeartInFreq					0x08

//****************** Register Constants ********************
// Register 0x00: Device type (R) 1 byte register Reads value 0x04 

// Register 0x01: Event Mask Register (R/W) 1 byte register
// Write to this register to have sent asynchronous messages to the EC based on various events 
#define	HomePosition1reached 									( 0x0001 << 0 )
#define	HomePosition2reached									( 0x0001 << 1 )
#define	HomePosition3reached									( 0x0001 << 2 )
#define	ErrorReachingDesiredPositionOnMotor1 					( 0x0001 << 3 )
#define	ErrorReachingDesiredPositionOnMotor2					( 0x0001 << 4 )
#define	PositionHome1Error										( 0x0001 << 5 )
#define	PositionHome2Error				 						( 0x0001 << 6 )
#define	PositionHome3Error										( 0x0001 << 7 )

// Register 0x02: Event Flag Register (R/W) 1 byte register
// Various bits are sent based on event type detected.
// A read clears the register.  
#define	HomePosition1reached 									( 0x0001 << 0 )
#define	HomePosition2reached									( 0x0001 << 1 )
#define	HomePosition3reached									( 0x0001 << 2 )
#define	ErrorReachingDesiredPositionOnMotor1 					( 0x0001 << 3 )
#define	ErrorReachingDesiredPositionOnMotor2					( 0x0001 << 4 )
#define	PositionHome1Error										( 0x0001 << 5 )
#define	PositionHome2Error				 						( 0x0001 << 6 )
#define	PositionHome3Error										( 0x0001 << 7 )

// Register 0x03: FW Version (R) 2 byte register  
// Byte 1: FW Major 
// Byte 2: FW Minor 

// Register 0x04: Aperture size 1 byte register 
// Value are between 0 (fully open) and 255(fully closed) 

// Register 0x05: Aperture orientation 1 byte register 
// Value are between 0 (fully open) and 255(fully closed) 

// Register 0x06: Calibration  1 byte register
// Writing to it forces the collimator to recalibrate
// Reading reports the calibration status:  
#define	CalibrationNotDone 		0
#define	CalibrationInProgress 1
#define	CalibrationCompleted 	2
#define	ErrorCalibrating  		3

// Register 0x07: Heartbeat Out Frequency (R/W) 1 byte register  Default value 0x00.  1 bit represents 10 milliseconds.  
// This register is set by the EC to set how often the heartbeat should be sent out.  
// To set a heartbeat frequency of 250ms set a value of 25 in the register.  
// The heartbeat message is simply sending out the heartbeat frequency.  

// Register 0x08: Heartbeat In Frequency (R/W) 1 byte register  Default value 0x00.  1 bit represents 10 milliseconds.  
// This register is set by the EC to set how often the heartbeat from the EC should arrive.  
// To set a heartbeat frequency of 250ms set a value of 25 in the register. 
// In case the heartbeat is not received within the expected time + 20%, the collimator leaves are 
// fully closed and stay closed until a power cycle.  

// Error reporting -----------------------------------------------------------------------
// In case an error is detected, the following messages are sent following the error byte.
#define	ChecksumError 						0x00
#define	CommandNotSupported					0x01
#define	InvalidParameterInCommand 			0x02
#define	CommandOutOfSequence 				0x03
#define	LengthDoesNotMatch					0x04

// ********************** State of the system  ***********************
#define	SYS_WAIT		0													// Command waiting
#define	SYS_SEEK1		1													// Moving 1st engine
#define	SYS_SEEK1OK		2													// Moving 1st engine complete
#define	SYS_SEEK2		3													// Moving 2st engine
#define	SYS_SEEK2OK		4													// Moving 2st engine complete
#define	SYS_CALIBR		5													// Calibration of 1st and 2nd engines
#define	SYS_ERROR		0x80												// System in error state

// Timings ------------------------------------------------------------------------------
#define	MAXTIMECALIBRATION	3000*10							// 3 ���
#define	MAXTIMESEEK			1000*10							// 1 ���
#define	MAXTIMEHEADIN		255*10*10						// 2550 ����
#define	MAXTIMEHEADOUT		255*10*10						// 2550 ����
#define	TLEDSYSMAX			400000

//****************** Function prototypes **********************
BYTE	GetChSum( BYTE* , BYTE );												// Calculating checksum modulo two
BYTE	GetChSumPlus( BYTE* , BYTE );											// Checksum calculation by simple summation

