#include "inc\stm32f10x.h"

#define	BYTE	unsigned char
#define	WORD	unsigned short
#define	DWORD	unsigned long

// ********************** System indicators  ***********************
#define	SYSLED			( 0x0001 << 4 )							// 0000 0000 0010 0000 - LED1 - PB4
#define	LedSysOn()	( GPIOB->ODR |= SYSLED )					// Turn on the LED
#define	LedSysOff()	( GPIOB->ODR &= (~SYSLED ) )				// Turn off the LED
#define	LedSysEx()	( GPIOB->ODR ^= SYSLED )					// Toggle LED

// ********************** Engine �1 *************************
#define	EN1					( 0x0001 << 2 )						// Work enable - PD2
#define	DIR1				( 0x0001 << 12 )					// Direction - PC12
#define	BRAKE1			( 0x0001 << 11 )						// Break - PA11

#define	Drv1En()		( GPIOD->ODR |= EN1 )					// On motor
#define	Drv1Dis()		( GPIOD->ODR &= (~EN1) )				// Off motor

#define	Drv1DirUp()	( GPIOC->ODR |= DIR1 )						// Rotate motor clockwise
#define	Drv1DirDn()	( GPIOC->ODR &= (~DIR1) )					// Rotate counterclockwise motor

#define	Drv1BrOn()	( GPIOA->ODR &= (~BRAKE1) )					// On brake
#define	Drv1BrOff()	( GPIOA->ODR |= BRAKE1 )					// Off brake

// ********************** Engine �2 *************************
#define	EN2					( 0x0001 << 2 )						// Work enable
#define	DIR2				( 0x0001 << 0 )						// Direction
#define	BRAKE2			( 0x0001 << 1 )							// Break 

#define	Drv2En()		( GPIOB->ODR |= EN2 )					// On motor
#define	Drv2Dis()		( GPIOB->ODR &= (~EN2) )				// Off motor

#define	Drv2DirUp()	( GPIOB->ODR |= DIR2 )						// Rotate motor clockwise
#define	Drv2DirDn()	( GPIOB->ODR &= (~DIR2) )					// Rotate counterclockwise motor

#define	Drv2BrOn()	( GPIOB->ODR &= (~BRAKE2) )					// On brake
#define	Drv2BrOff()	( GPIOB->ODR |= BRAKE2 )					// Off brake

// ********************* Home switch optocouplers **********************
#define	OPTOSENS1		( 0x0001 << 3 )
#define	OPTOSENS2		( 0x0001 << 1 )
#define	OPTOSENS3		( 0x0001 << 2 )

#define	Opto1In()	( GPIOC->IDR & OPTOSENS1 )					// Read the status of the optocouplers 1
#define	Opto2In()	( GPIOC->IDR & OPTOSENS2 )					// Read the status of the optocouplers 2
#define	Opto3In()	( GPIOC->IDR & OPTOSENS3 )					// Read the status of the optocouplers 3

// ******************** Function prototypes *************************
int	PortInit( void );											// Initialization of ports and pins

