#include "inc\stm32f10x.h"
#include "stdlib.h"

// **********************      Definitions      ***********************
#define	BYTE	unsigned char		//u8
#define	WORD	unsigned short		//u16
#define	DWORD	unsigned long		//u32
	
//******************    Function prototypes    **********************
int	SysClkInit( void );
int	SysTickInit( void );
void	EnableIRQ( BYTE );

void	Delay_ms( WORD );
void	Delay_s( WORD );


