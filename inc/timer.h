#include "inc\stm32f10x.h"
#include "stdlib.h"

// **********************      Definitions      ***********************
#define	BYTE	unsigned char		//u8
#define	WORD	unsigned short		//u16
#define	DWORD	unsigned long		//u32
	
// ********************** Encoder Constants *********************
#define	IPM_PER_INC1		1										// Number of encoder pulses per count
#define	IPM_PER_INC2		1
#define	IPM_PER_INC3		1
#define	IPM_PER_INC4		1
	
//****************** Function prototypes **********************
int		Tim1Init( void );											// Timer initialization 1

int		Tim2Init( void );											// Timer initialization 2
int		Tim3Init( void );											// Timer initialization 3
int		Tim4Init( void );											// Timer initialization 4
int		Tim5Init( void );											// Timer initialization 5

