#define	BYTE	unsigned char		//u8
#define	WORD	unsigned short		//u16
#define	DWORD	unsigned long		//u32

#include "stm32f10x.h"
#include "stdlib.h"

// ************************ Protocol constants ***************************
#define	MAXLENPACK	257						// Maximum package length
#define	STX		0xC0						// STX (start of byte)

// ******************** Constants for work buffer ************************
#define	BUFFFULL	1						// Buffer full
#define	BUFFEMPTY	-1						// Buffer empty

// ************************ Operating mode USART ****************************
#define	U_WAIT		0x00					// Waiting for a package
#define	U_START		0x01					// STX byte received
#define	U_RPACK		0x02					// Received package
#define	U_SPACK		0x03					// Packet transfer
#define	U_COMPL		0x04					// Package processed
#define	U_ERROR		0x70					// Package error


// ************************ Protocol commands ***************************
#define	COP_READ		0x00				// Read the register
#define	COP_WRITE		0x01				// Write register
#define	COP_ERROR		0x10				// Error
#define	COP_DEBUG		0xF0				// Debug


//****************************************************************************
//************************** Function prototypes *****************************
//****************************************************************************
int	USART1Init(void);											// Initialization USART1
int	USART2Init(void);											// Initialization USART2
int	USART3Init(void);											// Initialization USART3

int	CommRdBuffWr( BYTE );										// Write Byte to Buffer
int	CommRdBuffRd( void );										// Read byte from buffer
int	CommBuff2Buff( BYTE*, int );								// Copy packet from port buffer to external buffer
