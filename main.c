//*******************************************************
//* The programm control Emitter Collimator PCB			*
//* Author: SlobodenukSK@gmail.com 2018-09-17			*
//*******************************************************
//* CPU STM32F103RCT6
//* without quartz, built-in rc generator, clock frequency 36���, debuger SerialWire(ST-Link)
//* Two motors in drivers L6235D:
//* - ENABLE1(PD2), DIR1(PC12), BRAKE1(PA11), Vref1 (PA10-TIM1CH3), �������1 (PA15-TIM2CH1 + PB3-TIM2CH2), �������2 (PA6-TIM3CH1 + PA7-TIM3CH2), �������� 1 (PC3)  
//* - ENABLE2(PB2), DIR2(PB0), BRAKE2(PB1), Vref2 (PA9-TIM1CH2), �������3 (PB6-TIM4CH1 + PB7-TIM4CH2), �������4 (PA0-TIM5CH1 + PA1-TIM5CH2), �������� 2 (PC1) 
//* System indicator - Led (PB4)
//* Home position - open channel optocouplers (Optosens1,2,3) (PC1,PC2,PC3)
//* Control USART3
//*******************************************************

#include <stdio.h>
#include <stdlib.h>
#include "inc\stm32f10x.h"
#include "inc\sysinit.h"
#include "inc\port_pin.h"
#include "inc\timer.h"
#include "inc\usart.h"
#include "inc\main.h"

// *********** External variables *****************
extern volatile	BYTE	Comm3BuffTx[];												// Buffer to send
extern volatile	BYTE	Comm3BuffTxCnt;												// Number of bytes in the send buffer
extern volatile	BYTE	Comm3BuffTxIdxSend;											// Pointer to the last byte transmitted

extern volatile	WORD	Comm3CtPacket;												// Packets in Comm3 input buffer

extern volatile	DWORD	TDelaySeek;													// Real-time variable - motor seek timeout
extern volatile	DWORD	TDelayCalibr;												// Real-time variable - calibrating timeout
extern volatile	DWORD	TDelayHeadIn;												// Real-time variable - poll timeout
extern volatile	DWORD	TDelayHeadOut;												// Real-time variable - response timeout

//****************************************************************************
// Global variables
//****************************************************************************
volatile	BYTE	SysMode = SYS_WAIT;												// Controller mode

// *********************** Controller registers *******************************
//volatile	BYTE	RegDeviceType = 0x04;										
volatile	BYTE	RegEventMask = 0x00;										
volatile	BYTE	RegEventFlag = 0x00;
//volatile	WORD	RegFWVersion = 0x0101;										
volatile	BYTE	RegMotor1Pos = 0x00;											// Current aperture (0-255)
volatile	BYTE	RegMotor1Pos2 = 0x00;											// Required Aperture (0-255)
volatile	BYTE	RegMotor2Pos = 0x00;											// Current orientation (0-255)
volatile	BYTE	RegMotor2Pos2 = 0x00;											// Required orientation (0-255)
volatile	BYTE	RegCalibration = 0x00;											// 
volatile	BYTE	RegHeartBeatOutF = 0x00;										// 
volatile	BYTE	RegHeartBeatInF = 0x00;											// 

// ************************* Code Replacement Bytes ***************************
#define	B_DB	0xDB							// 
#define	B_DC	0xDC							// 
#define	B_DD	0xDD							// 

BYTE	CommandBuff[MAXLENPACK];													// Buffer to process the current command
WORD	CommandBuffLen = 0;															// Length of package for processing
BYTE	PacketError = 0;															// Package error
BYTE	PacketChSum = 0;															// Checksum
BYTE	numSTX = 0;

//****************************************************************************
// Main Function
//****************************************************************************
int main (void) {
	DWORD	ich = 0;																		// counter for SYSLED
	int		bt;
	BYTE	n;
	
	SysClkInit();                		             										// Initialize syst. generator and PLL
	SysTickInit();                             			    								// System Timer Initialization
	
	PortInit();																				// Port initialization

	// Test SYSLED
	LedSysOn();
	Delay_ms( 125 );
	LedSysOff();
	Delay_ms( 125 );
	
	USART3Init();																			// Initialization USART3

	Tim1Init();																				// Initialization TIM1 for PWM Vref1, Vref2
	Tim2Init();																				// Initialization TIM2 for encoder
	Tim3Init();																				// Initialization TIM3 for encoder
	Tim4Init();																				// Initialization TIM4 for encoder
	Tim5Init();																				// Initialization TIM5 for encoder

	// Initialization hardware complete --------------------------------------------------------
	
	//RegCalibration = CalibrationNotDone;													// 
	
	SysMode = SYS_WAIT;																		// Standby system

	//---------------- Main look ------------------
	while(1){	
		if( ++ich == TLEDSYSMAX ){																// Toggle LED to indicate work
			ich = 0;
			LedSysEx();																						
		}
	 
		// If there are packets in the buffer -----------------------------------------------------------------------
		if( Comm3CtPacket ){																	
			LedSysEx();																				// Toggle LED to indicate exchange
						
			// Get package from input buffer with transform
			CommandBuffLen = 0;																		// Reset the packet length counter
			PacketError = 0;																		// Reset error
			PacketChSum = 0;																		// Reset checksum
			
			while( 1 ){
				
				bt = CommRdBuffRd();																// Read package data from clipboard
				
				switch( bt ){
					
					case STX:{																		// If STX
						numSTX++;																		// Increase counter STX
					} break;
					
					case B_DB:{																		// Code Replacement
						bt = CommRdBuffRd();
						if( bt == B_DC )
							bt = 0xC0;																	// Code Replacement 0xC0
						else if( bt == B_DD )
							bt = 0xDB;																	// Code Replacement 0xDB
						else{
							PacketError = 1;															// !!! PACKAGE ERROR !!! not correct code replacement
							break;
						} 
					} break;
				
				}

				CommandBuff[CommandBuffLen++] = bt;												// Transfer data
				
				if( numSTX == 2 ){																// If the package is accepted
					numSTX = 0;																		// - reset counter STX
					Comm3CtPacket--;																// - reduce the number of packets in the buffer
					break;
				}
			}

			// Package analysis -------------------------------------------------------------------------------
			if( CommandBuff[1] != CommandBuffLen - 2 ){											// - check length packet
				PacketError = 2;																			
			}else{
				PacketChSum = GetChSumPlus( &CommandBuff[1], CommandBuff[1]-1 );				// - calculate checksum	
				
				PacketChSum ^= CommandBuff[CommandBuff[1]];
				if( PacketChSum ){																// - check checksum
					PacketError = 3;
				}
			}
			
			// If the package is correct ----------------------------------------------------------------------
			if( !PacketError ){
				// Execute the command
				// STX(0xC0), Length, Seq#, Cmd, Reg, Data, CS, ETX(0xC0)
				//							|- Read (0x00) Write (0x01) Error(0x02) Debug(0xF0)
				//										|- 
				// 0xC0 0x05 0x00 0x00 0x00 0xFB 0xC0 - ������ Device type
				
				switch( CommandBuff[3] ){
					
					// Read controller register --------------------------------------------------
					case COP_READ:{
						// 	Prepare response based on input packet
						for( n = 1; n < CommandBuffLen; n++ )
							Comm3BuffTx[n-1] = CommandBuff[n];
						
						switch( CommandBuff[4] ){
							
							// Read controller type (0x00) ---------------------------------------------------------
							case R_DeviceType:{
								
								Comm3BuffTx[1]++;																	// Seq++
								Comm3BuffTx[4]= 0x04;																// Controller Type - Collimator Controller (0x04)
								Comm3BuffTx[5] = GetChSumPlus( (BYTE*)Comm3BuffTx, 5 );								// Checksum for packet
								Comm3BuffTx[6] = 0xC0;
								
								Comm3BuffTxIdxSend = 0;
								Comm3BuffTxCnt = 7;
								USART3->DR = 0xC0;																	// - send packet (STX)
								
							} break;
							// ----------------------------------------------------------------------------------------
							
							// Read controller event mask register (0x01) ---------------------------------------
							case R_EventMask:{
								Comm3BuffTx[1]++;																	// Seq++
								Comm3BuffTx[4] = RegEventMask;														// Event Mask Register
								Comm3BuffTx[4] = 0x00;
								Comm3BuffTx[5] = GetChSumPlus( (BYTE*)Comm3BuffTx, 5 );								// Checksum for packet
								Comm3BuffTx[6] = 0xC0;
								
								Comm3BuffTxIdxSend = 0;
								Comm3BuffTxCnt = 7;
								USART3->DR = 0xC0;																	// - send packet (STX)
							} break;
							// ----------------------------------------------------------------------------------------
							
							// Reading the register of controller event flags (0x02) --------------------------------------
							case R_EventFlag:{
								Comm3BuffTx[1]++;																	// Seq++
								Comm3BuffTx[4] = RegEventFlag;														// ������� ������ �������
								Comm3BuffTx[5] = GetChSumPlus( (BYTE*)Comm3BuffTx, 5 );								// Checksum for packet
								Comm3BuffTx[6] = 0xC0;
								
								Comm3BuffTxIdxSend = 0;
								Comm3BuffTxCnt = 7;
								USART3->DR = 0xC0;																	// - send packet (STX)
								
								RegEventFlag = 0x00;																// A read clears the register.
							} break;
							// ----------------------------------------------------------------------------------------
							
							// Read firmware version (0x03) ----------------------------------------------------------
							case R_FWVersion:{
								
								Comm3BuffTx[0]++;																	// Increase the length of the response byte
								Comm3BuffTx[1]++;																	// Seq++
								Comm3BuffTx[4] = 0x01;																// FW Major (0x01)
								Comm3BuffTx[5] = 0x01;																// FW Minor (0x01)
								Comm3BuffTx[6] = GetChSumPlus( (BYTE*)Comm3BuffTx, 6 );								// Checksum for packet
								Comm3BuffTx[7] = 0xC0;
								
								Comm3BuffTxIdxSend = 0;
								Comm3BuffTxCnt = 8;
								USART3->DR = 0xC0;																	// - send packet (STX)
								
							} break;
							// ----------------------------------------------------------------------------------------
							
							// Reading the aaperture register (0x04) -------------------------------------------------------
							case R_ApertureSize:{
								Comm3BuffTx[1]++;																	// Seq++
								Comm3BuffTx[4] = RegMotor1Pos;														// Aperture Register
								Comm3BuffTx[5] = GetChSumPlus( (BYTE*)Comm3BuffTx, 5 );								// Checksum for packet
								Comm3BuffTx[6] = 0xC0;
								
								Comm3BuffTxIdxSend = 0;
								Comm3BuffTxCnt = 7;
								USART3->DR = 0xC0;																	// - send packet (STX)
							
							} break;
							// ----------------------------------------------------------------------------------------
							
							// Reading the orientation register (0x05) ------------------------------------------------------
							case R_ApertureOrientation:{
								Comm3BuffTx[1]++;																	// Seq++
								Comm3BuffTx[4] = RegMotor2Pos;														// Orientation register
								Comm3BuffTx[5] = GetChSumPlus( (BYTE*)Comm3BuffTx, 5 );								// Checksum for packet
								Comm3BuffTx[6] = 0xC0;
								
								Comm3BuffTxIdxSend = 0;
								Comm3BuffTxCnt = 7;
								USART3->DR = 0xC0;																	// - send packet (STX)
							} break;
							// ----------------------------------------------------------------------------------------
							
							// Read calibration register (0x06) ------------------------------------------------------
							case R_Calibration:{
								Comm3BuffTx[1]++;																	// Seq++
								Comm3BuffTx[4] = RegCalibration;													// Calibration Register
								Comm3BuffTx[5] = GetChSumPlus( (BYTE*)Comm3BuffTx, 5 );								// Checksum for packet
								Comm3BuffTx[6] = 0xC0;
								
								Comm3BuffTxIdxSend = 0;
								Comm3BuffTxCnt = 7;
								USART3->DR = 0xC0;																	// - send packet (STX)
							} break;
							// ----------------------------------------------------------------------------------------

							// Reading the register output frequencies (0x07) -----------------------------------------
							case R_HeartOutFreq:{
								Comm3BuffTx[1]++;																	// Seq++
								Comm3BuffTx[4] = RegHeartBeatOutF;													// Register output frequency
								Comm3BuffTx[5] = GetChSumPlus( (BYTE*)Comm3BuffTx, 5 );								// Checksum for packet
								Comm3BuffTx[6] = 0xC0;
								
								Comm3BuffTxIdxSend = 0;
								Comm3BuffTxCnt = 7;
								USART3->DR = 0xC0;																	// - send packet (STX)
							} break;
							// ----------------------------------------------------------------------------------------
							
							// Reading the input frequency register (0x08) --------------------------------------------
							case R_HeartInFreq:{
								Comm3BuffTx[1]++;																	// Seq++
								Comm3BuffTx[4] = RegHeartBeatInF;													// Register input frequency
								Comm3BuffTx[5] = GetChSumPlus( (BYTE*)Comm3BuffTx, 5 );								// Checksum for packet
								Comm3BuffTx[6] = 0xC0;
								
								Comm3BuffTxIdxSend = 0;
								Comm3BuffTxCnt = 7;
								USART3->DR = 0xC0;																	// - send packet (STX)
							} break;
							// ----------------------------------------------------------------------------------------
							
							// Invalid register number ---------------------------------------------------------------
							default:
								PacketError = 5;
						}

					} break;
					// ------------------------------------------------------------------------------
					
					// Controller register entry --------------------------------------------------
					case COP_WRITE:{
						
						// Execute the command
						switch( CommandBuff[4] ){
							
							// Write controller event mask register (0x01) ---------------------------------------
							case R_EventMask:{
								RegEventMask = CommandBuff[5];														// Event Mask Register
							} break;
							// ----------------------------------------------------------------------------------------
							
							// Writing a register of controller event flags (0x02) ------------------------------------
							case R_EventFlag:{
									RegEventFlag = CommandBuff[5];													// Register of event flags
							} break;
							// ----------------------------------------------------------------------------------------
							
							// Write in the aperture register (0x04) --------------------------------------------------------
							case R_ApertureSize:{
								RegMotor1Pos2 = CommandBuff[5];														// Save required aperture value
								
								// Calculate the value of encoder pulses
								if( RegMotor1Pos2 > RegMotor1Pos ){													// Forward movement
									TIM4->CNT = 0xFFFF - RegMotor1Pos2 * IPM_PER_INC1;
									Drv1DirUp();
								}else{
									TIM4->CNT = RegMotor1Pos2 * IPM_PER_INC1;										// Backward movement
									Drv1DirDn();
								}
								
								TDelaySeek = MAXTIMESEEK;															// Set Seek Timeout
								
								Drv1En();																			// Start the engine
								Drv1BrOff();																		// Release the brake
								
								SysMode = SYS_SEEK1;																// Enable move mode 1
							} break;
							// ----------------------------------------------------------------------------------------
							
							// Write orientation register (0x05) ------------------------------------------------------
							case R_ApertureOrientation:{
								RegMotor2Pos2 = CommandBuff[5];														// Save required orientation value
								
								// Calculate the value of encoder pulses
								if( RegMotor2Pos2 > RegMotor2Pos ){													// Forward movement
									TIM5->CNT = 0xFFFF - RegMotor2Pos2 * IPM_PER_INC1;
									Drv1DirUp();
								}else{
									TIM5->CNT = RegMotor2Pos2 * IPM_PER_INC1;										// Backward movement
									Drv1DirDn();
								}
								
								TDelaySeek = MAXTIMESEEK;															// Set Seek Timeout
								
								Drv2En();																			// Start the engine
								Drv2BrOff();																		// Release the brake
								
								SysMode = SYS_SEEK2;																// Enable move mode 2
							} break;
							// ----------------------------------------------------------------------------------------
							
							// Write calibration register (0x06) ------------------------------------------------------
							case R_Calibration:{
								// Start calibration procedure
								TDelayCalibr = MAXTIMECALIBRATION;												// Set Calibration Timeout
								RegCalibration = CalibrationInProgress;											// Set the calibration process flag
								
								Drv1En();																		// Allow both engines
								Drv2En();
								
								Drv1DirDn();																	// - on engine 1 reverse rotation on min. speeds
								Drv1BrOff();
								
								Drv2DirDn();																	// - on engine 2 reverse rotation on min. speeds
								Drv2BrOff();
								
								SysMode = SYS_CALIBR;															// Start calibration
							} break;
							// ----------------------------------------------------------------------------------------

							// Write register out frequencies (0x07) ----------------------------------------------------
							case R_HeartOutFreq:{
								RegHeartBeatOutF = CommandBuff[5];												// Register out frequencies
							} break;
							// ----------------------------------------------------------------------------------------
							
							// Write register In frequencies (0x07) -----------------------------------------------------
							case R_HeartInFreq:{
								RegHeartBeatInF = CommandBuff[5];												// Register in frequencies
							} break;
							// ----------------------------------------------------------------------------------------
							
							// Invalid register number ---------------------------------------------------------------
							default:
								PacketError = 5;						
						}
						
						if( !PacketError ){
							// Prepare an answer, but with a modified seg# field and a checksum
							for( n = 1; n < CommandBuffLen; n++ )
								Comm3BuffTx[n-1] = CommandBuff[n];									// copy buffer
							Comm3BuffTx[1]++;														// seq++
							Comm3BuffTx[CommandBuffLen-3] = GetChSumPlus( (BYTE*)Comm3BuffTx, CommandBuffLen - 3 );
							
							Comm3BuffTxIdxSend = 0;
							Comm3BuffTxCnt = CommandBuffLen-1;
							
							USART3->DR = 0xC0;													// - Send packet
						}
						
					} break;
					// ------------------------------------------------------------------------------
					
					// Service command ------------------------------------------------------------
					case COP_DEBUG:{
						switch( CommandBuff[4] ){
							case	0x00:{
								Drv1DirUp();
								USART3->DR = 0xF1;												// - send Fn
							} break;
							
							case	0x01:{
								Drv1DirDn();
								USART3->DR = 0xF2;												// - send Fn
							} break;
							
							case	0x02:{
								Drv1BrOff();
								USART3->DR = 0xF3;												// - send Fn
							} break;
							
							case	0x03:{
								Drv1BrOn();
								USART3->DR = 0xF4;												// - send Fn
							} break;
							
							case	0x04:{
								Drv1En();
								USART3->DR = 0xF5;												// - send Fn
							} break;
							
							case	0x05:{
								Drv1Dis();
								USART3->DR = 0xF6;												// - send Fn
							} break;
							
							case	0x06:{
								if( TIM1->CCR2 < 1000 ){
									TIM1->CCR2 += 10;											// Durability PWM motor 1 inc
								}
								USART3->DR = 0xF7;												// - send Fn
							} break;
							
							case	0x07:{
								if( TIM1->CCR2 > 10 ){
									TIM1->CCR2 -= 10;											// Durability PWM motor 1 dec
								}
								USART3->DR = 0xF8;												// - send Fn
							} break;
							
							case	0x08:{
								Drv2DirUp();
								USART3->DR = 0xF1;												// - send Fn
							} break;
							
							case	0x09:{
								Drv2DirDn();
								USART3->DR = 0xF2;												// - send Fn
							} break;
							
							case	0x0A:{
								Drv2BrOff();
								USART3->DR = 0xF3;												// - send Fn
							} break;
							
							case	0x0B:{
								Drv2BrOn();
								USART3->DR = 0xF4;												// - send Fn
							} break;
							
							case	0x0C:{
								Drv1En();
								Drv2En();
								USART3->DR = 0xF5;												// - send Fn
							} break;
							
							case	0x0D:{
								Drv2Dis();
								USART3->DR = 0xF6;												// - send Fn
							} break;
							
							case	0x0E:{
								if( TIM1->CCR2 < 1000 ){
									TIM1->CCR3 += 10;											// Durability PWM motor 2 inc
								}
								USART3->DR = 0xF7;												// - send Fn
							} break;
							
							case	0x0F:{
								if( TIM1->CCR2 > 10 ){
									TIM1->CCR3 -= 10;											// Durability PWM motor 2 dec
								}
								USART3->DR = 0xF8;												// - send Fn
							} break;

							default:
								USART3->DR = 0xF0;												// - send Fn
						}
					} break;
					// ------------------------------------------------------------------------------
					
					// Unknown command ----------------------------------------------------------
					default:{
						PacketError = 4;
					}
					// ------------------------------------------------------------------------------
				}
			}
			
			// If the package with an error ------------------------------------------------------------------------
			if( PacketError ){
				
				// Generate an error response packet
				switch( PacketError ){
					case 1:{
						Comm3BuffTx[3] = CommandOutOfSequence;										// Incorrect code replacement
					} break;
					case 2:{																							
						Comm3BuffTx[3] = LengthDoesNotMatch;										// Not true packet length
					} break;
					case 3:{																							
						Comm3BuffTx[3] = ChecksumError;												// Invalid checksum
					} break;
					case 4:{
						Comm3BuffTx[3] = CommandNotSupported;										// Command not supported
					} break;
					case 5:{
						Comm3BuffTx[3] = InvalidParameterInCommand;									// Invalid command parameters
					} break;
				}
				
				Comm3BuffTx[0] = 0x05;																// Length
				Comm3BuffTx[1] = CommandBuff[2]+1;													// Seq++
				Comm3BuffTx[2] = 0x10;																// Packet Error
				Comm3BuffTx[4] = GetChSumPlus( (BYTE*)Comm3BuffTx, 4 );								// Checksum
				Comm3BuffTx[5] = 0xC0;																// ETX
				
				Comm3BuffTxIdxSend = 0;
				Comm3BuffTxCnt = 6;
				
				// ������ �����
				USART3->DR = 0xC0;																	// STX
			}
			
		}
		// -----------------------------------------------------------------------------------
		
		// Calibration ------------------------------------------------------------------------
		if( SysMode == SYS_CALIBR ){
			
			if( TDelayCalibr ){																		// If there is time
				
				// Motor 1 - wait for the home switch 1
				if( Opto1In() == 0 ){
					Drv1BrOn();																						// - on engine brake 1
					Drv1Dis();																						// - off engine 1
					RegEventFlag |= HomePosition1reached;															// - set flag HomePosition1reached
				}
				
				// Motor 2 - wait for the home switch 2
				if( Opto2In() == 0 ){
					Drv2BrOn();																						// - on engine brake 2
					Drv2Dis();																						// - off engine 2
					RegEventFlag |= HomePosition2reached;															// - set flag HomePosition2reached
				}
				
				// If both home switches are reached
				if( ( Opto1In() == 0 ) && ( Opto2In() == 0 ) ){
				  	RegCalibration = CalibrationCompleted;															// - set flag calibration completed
					SysMode = SYS_WAIT;																				// - go into standby mode
				}
				
			}else{																								// If time is up 
				Drv1BrOff();																						// - turn on the brakes
				Drv2BrOff();
				Drv1Dis();																							// - turn off the engines
				Drv2Dis();
				
				RegCalibration = ErrorCalibrating;																	// - set calibration error flag
				if( Opto1In() )																						// if home switches 1 are not reached
				  RegEventFlag |= PositionHome1Error;																// - set flag search error home position 1
				if( Opto2In() )																						// if home switches 2 are not reached
				  RegEventFlag |= PositionHome2Error;																// - set flag search error home position 2
				
				SysMode = SYS_WAIT;																					// We are waiting for the next command
			}
		}
		// -----------------------------------------------------------------------------------
		
		// Seek engines ----------------------------------------------------------------------
		
		// Aperture change 
		if( SysMode == SYS_SEEK1OK ){																			// If positioning is complete
			RegMotor1Pos = RegMotor1Pos2;																			// Save current position
			SysMode = SYS_WAIT;																						// Go to standby
		}
		
		// Orientation change 
		if( SysMode == SYS_SEEK2OK ){																			// If positioning is complete
			RegMotor2Pos = RegMotor2Pos2;																			// Save current position
			SysMode = SYS_WAIT;																						// Go to standby
		}

		// Error seek 
		if( ( SysMode == SYS_SEEK1 )&&( TDelaySeek == 0 ) ){													// If time to move out
		  	RegEventFlag = ErrorReachingDesiredPositionOnMotor1;													// Set flag move error
			Drv1BrOff();																							// - turn on the brakes
			Drv1Dis();																								// - turn off the engines

			SysMode = SYS_WAIT;	
		}
		
		if( ( SysMode == SYS_SEEK2 )&&( TDelaySeek == 0 ) ){													// If time to move out
		  	RegEventFlag = ErrorReachingDesiredPositionOnMotor2;													// Set flag move error
			Drv2BrOff();																							// - turn on the brakes
			Drv2Dis();																								// - turn off the engines

			SysMode = SYS_WAIT;	
		}
		// -----------------------------------------------------------------------------------
		

	}
	
}

// ********************************************************************
// ********************* Checksum calculation *************************
// ********************************************************************
BYTE	GetChSum( BYTE *pb, BYTE len ){
	BYTE	chSum = 0;
	BYTE	n;
	
	for( n = 0; n < len; n++ )
		chSum ^= pb[n];

	chSum = (~chSum) + 1;

	return chSum;
}

BYTE	GetChSumPlus( BYTE *pb, BYTE len ){
	BYTE	chSum = 0;
	BYTE	n;
	
	for( n = 0; n < len; n++ )
		chSum += pb[n];

	chSum = (~chSum) + 1;

	return chSum;
}
