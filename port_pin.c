#include "inc\port_pin.h"

//****************************************************************
//******************** Port configuration ************************
//****************************************************************
int	PortInit( void ){
	
	// ----- Enable Port Clocking -----------------------------------
	RCC->APB2ENR |= RCC_APB2ENR_IOPAEN;			// Enable Port A Clocking
	RCC->APB2ENR |= RCC_APB2ENR_IOPBEN;			// Enable Port B Clocking
	RCC->APB2ENR |= RCC_APB2ENR_IOPCEN;			// Enable Port C Clocking
	RCC->APB2ENR |= RCC_APB2ENR_IOPDEN;			// Enable Port D Clocking
	RCC->APB2ENR |= RCC_APB2ENR_AFIOEN;			// On clocking alternative GPIO functions.
	
	// Configure Port A -----------------------------------------------------------
	// PA0 - TIM5_CH1 - input A of encoder 4
	// PA1 - TIM5_CH2 - input B of encoder 4
	// PA2,PA3, PA4, PA5 - na
	// PA6 - TIM3_CH1 - input A of encoder 2
	// PA7 - TIM3_CH2 - input B of encoder 2
	// PA8 - na
	// PA9 - TIM1_CH2 - Vref2 ( PWM TIM1 )
	// PA10 - TIM1_CH3 - Vref1 ( PWM TIM1 )
	// PA11 - BREAK1
	// PA12 - na
	// SWD: PA13, PA14
	// PA15 - TIM2_CH1 - input A of encoder 1
	GPIOA->CRL = 0x44444444;				// 0100 0100 0100 0100 0100 0100 0100 0100
	GPIOA->CRH = 0x44442994;				// 0100 0100 0100 0100 0010 1001 1001 0100
	GPIOA->ODR = 0x80C3;						// 1000 0000 1100 0011
	
	// Configure Port B ------------------------------------------------------------
	// PB0 - DIR2
	// PB1 - BREAK2
	// PB2 - EN2
	// PB3 - TIM2_CH2 - input B of encoder 1
	// PB4 - SYSLED
	// PB5 - na
	// PB6 - TIM4_CH1 - input A of encoder 3
	// PB7 - TIM4_CH2 - input B of encoder 3
	// PB8, PB9 - na
	// PB10 - USART3_TX
	// PB11 - USART3_RX
	// PB12-PB15 - na
	GPIOB->CRL = 0x44424222;				// 0100 0100 0100 0010 0100 0010 0010 0010
	GPIOB->CRH = 0x44444944;				// 0100 0100 0100 0100 0100 1001 0100 0100
	GPIOB->ODR = 0x00C8;						// 0000 0000 1100 1000
	
	// Configure Port C ------------------------------------------------------------
	// PC0 - na
	// PC1 - Optosens2
	// PC2 - Optosens3
	// PC3 - Optosens1
	// PC4 - PC11 - na
	// PC12 - DIR1
	// PC13, PC14, PC15 - na
	GPIOC->CRL = 0x44444444;				// 0100 0100 0100 0100 0100 0100 0100 0100
	GPIOC->CRH = 0x44224444;				// 0100 0100 0100 0010 0100 0100 0100 0100
	GPIOC->ODR = 0x0000;						// 0000 0000 0000 0000
	
	// Configure Port D ------------------------------------------------------------
	// PD0, PD1, PD3-PD15 - na
	// PD2 - EN1
	GPIOD->CRL = 0x44444244;				// 0100 0100 0100 0100 0100 0010 0100 0100
	GPIOD->CRH = 0x44444444;				// 0100 0100 0100 0100 0100 0100 0100 0100
	GPIOD->ODR = 0x0000;						// 0000 0000 0000 0000
	// -----------------------------------------------------------------------------
	
	//------------------------- Remaping and debugging --------------------------
	AFIO->MAPR |= AFIO_MAPR_SWJ_CFG_0;							// Disable JTAG debugger (leaving SWD)
	AFIO->MAPR |= AFIO_MAPR_TIM2_REMAP_0;						// TIM2 out pin PA15 PB3

	return 0;
}
						

