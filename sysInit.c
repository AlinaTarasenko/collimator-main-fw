#include "inc\sysinit.h"

volatile	DWORD	TLedSys = 0;							// Real-time variable - Led delay
volatile	DWORD	TDelay1 = 0;							// Real-time variable - daley also
volatile	DWORD	TDelaySeek = 0;							// Real-time variable - motor seek timeout
volatile	DWORD	TDelayCalibr = 0;						// Real-time variable - calibrating timeout
volatile	DWORD	TDelayHeadIn = 0;						// Real-time variable - poll timeout
volatile	DWORD	TDelayHeadOut = 0;						// Real-time variable - response timeout

//****************************************************************
//****************** Initialize syst. generator ******************
//****************************************************************
int	SysClkInit( void ){
	DWORD	t1;
 
	// Use built-in HSI generator
	 
	//--------------------------  PLL ----------------------------
	RCC->CFGR |= RCC_CFGR_PLLSRC_HSI_Div2;											// Clock PLL from HSI / 2
	RCC->CFGR |= RCC_CFGR_PLLMULL_2 | RCC_CFGR_PLLMULL_1 | RCC_CFGR_PLLMULL_0;		// 8MHz / 2 * 9 = 36MHz
	 
	RCC->CR |= RCC_CR_PLLON;														// Enable PLL clocking
	for( t1 = 0; t1 < 50000; t1++ ){ 												// Wait PLL ready
		if( RCC->CR & RCC_CR_PLLRDY )
			break;
	}
	 
	if( RCC->CR & RCC_CR_PLLRDY ){													// If the PLL starts up and is ready
			
		FLASH->ACR |= FLASH_ACR_LATENCY_1;											// Flash access time - One wait state
		 
		//------------------ Setting frequencies for device buses ------------------
		RCC->CFGR |= RCC_CFGR_HPRE_DIV1;											// AHB divider = 1
		RCC->CFGR |= RCC_CFGR_PPRE1_DIV1;											// APB1 divider = 1
		RCC->CFGR |= RCC_CFGR_PPRE2_DIV1;											// APB2 divider = 1
	 
		// Enable clocking from PLL
		RCC->CFGR &= ~RCC_CFGR_SW;													// Clear SW0, SW1
		RCC->CFGR |= RCC_CFGR_SW_PLL;												// Select PLL for clocking - 36MHz
		 
	}else{
		return 2;																	// PLL Initialization Error
	}
	 
	 
	return 0;
}

//****************************************************************
//******************** System timer setting **********************
//****************************************************************
int	SysTickInit( void ){
	// interrupt every 100 ��� = 36000000/8/450 = 10000 ���/���
	SysTick->VAL = 450;
	SysTick->LOAD = 450;
	
	// Enable SysTick IRQ and SysTick Timer
	SysTick->CTRL  = //SysTick_CTRL_CLKSOURCE_Msk |
                   SysTick_CTRL_TICKINT_Msk   |
                   SysTick_CTRL_ENABLE_Msk;
	
	return 0;
}

//****************************************************************
//*************** System Timer Interrupt Handler  ****************
//****************************************************************
void	SysTick_Handler( void ){
	
	// Delays -------------------------------------------------------
	if( TDelay1 )											// If the value is not 0
		TDelay1--;											// - reduce by one
	
	if( TDelaySeek )										// Real-time variable - motor seek timeout
		TDelaySeek--;
	
	if( TDelayCalibr )										// Real-time variable - calibrating timeout
		TDelayCalibr--;
	
	if( TDelayHeadIn )										// Real-time variable - poll timeout
		TDelayHeadIn--;								
	
	if( TDelayHeadOut )										// Real-time variable - response timeout
		TDelayHeadOut;
	//----------------------------------------------------------------------------
}

//**************** Runtime function *******************

// Delay millisecond
void	Delay_ms( WORD tDelay ){
	for( TDelay1 = tDelay*10; TDelay1 !=0; );
}

// Second delay
void	Delay_s( WORD tDelayS ){
	for( TDelay1 = tDelayS * 10000; TDelay1 !=0; );
}
//****************************************************************

//***************************************************************
//****************** Enable Interrupts NVIC *********************
//***************************************************************
void	EnableIRQ( BYTE IntNum ){
 if( IntNum <= 31 )
 	NVIC->ISER[0] = 1 << IntNum;
 if( (IntNum > 31)&&(IntNum <= 63 ) )
 	NVIC->ISER[1] = 1 << ( IntNum-32 );
 if( (IntNum > 63)&&(IntNum <= 95 ) )
 	NVIC->ISER[2] = 1 << ( IntNum-64 );
}

