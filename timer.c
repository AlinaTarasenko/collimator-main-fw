#include "inc\timer.h"
#include "inc\main.h"
#include "inc\port_pin.h"
#include "inc\sysinit.h"

extern	volatile	BYTE	SysMode;												// Controller mode

//****************************************************************
//************************* Timer �1 ****************************
//****************************************************************
// PWM channels 2 and 3
int	Tim1Init(){
	RCC->APB2ENR |= RCC_APB2ENR_TIM1EN;												// Enable 1st timer clocking
	
	TIM1->PSC = 0x0001;																// divider 1
	TIM1->CNT = 0x0000;																// Counter Up
	TIM1->ARR = 1000;																// Reloadable value
	
	TIM1->CCR2 = 100;																// PWM channel 2
	TIM1->CCR3 = 100;																// PWN channel 3
	
	TIM1->CCMR1 |= TIM_CCMR1_OC2M_2 | TIM_CCMR1_OC2M_1;
	TIM1->CCMR2 |= TIM_CCMR2_OC3M_2 | TIM_CCMR2_OC3M_1;
	
	TIM1->CCER |= TIM_CCER_CC2E | TIM_CCER_CC3E;									// Enable outputs 2 and 3
	TIM1->BDTR |= TIM_BDTR_MOE;														// Enable all output
	
	TIM1->CR1 = TIM_CR1_CEN;														// Start timer
	
	return 0;
}



//****************************************************************
//************************* Timer �2 ****************************
//****************************************************************
int	Tim2Init(){
	RCC->APB1ENR |= RCC_APB1ENR_TIM2EN;												// Enable 2st timer clocking

	// Configure the 2nd multiplexer for the 2nd input ( IC2 = TI2 )
	// Configuring the 1st multiplexer for the 1st input ( IC1 = TI1 )
	// without filter and prescaler
	TIM2->CCMR1 = TIM_CCMR1_CC2S_0 | TIM_CCMR1_CC1S_0;

	// Capture on falling down imp. (CC1P=CC2P=1)
	// no counter capture in register (CC1E=CC2E=0)
	TIM2->CCER = TIM_CCER_CC1P | TIM_CCER_CC2P;	
	
	// On Encoder interface: TI1FP1 transition and TI2FP2 transition account
	TIM2->SMCR = TIM_SMCR_SMS_0 | TIM_SMCR_SMS_1;
	
	
	TIM2->CNT = 0x8000;																// Score up / down from the middle
	TIM2->ARR = 0x8000;																// Reloadable value
	
	TIM2->DIER = TIM_DIER_UIE;														// Enable Timer Overflow Interrupts
	
	TIM2->CR1 = TIM_CR1_CEN;														// Start timer
	
	return 0;
}


void TIM2_IRQHandler(void){
	TIM2->SR &= ~TIM_SR_UIF; 														// Clear flag UIF

}

//****************************************************************
//************************* Timer �3 ****************************
//****************************************************************
int	Tim3Init(){
	RCC->APB1ENR |= RCC_APB1ENR_TIM3EN;												// Enable 3st timer clocking
	
	// Configure the 2nd multiplexer for the 2nd input ( IC2 = TI2 )
	// Configuring the 1st multiplexer for the 1st input ( IC1 = TI1 )
	// without filter and prescaler
	TIM3->CCMR1 = TIM_CCMR1_CC2S_0 | TIM_CCMR1_CC1S_0;
	
	// Capture on falling down imp. (CC1P=CC2P=1)
	// no counter capture in register (CC1E=CC2E=0)
	TIM3->CCER = TIM_CCER_CC1P | TIM_CCER_CC2P;
	
	// On Encoder interface: TI1FP1 transition and TI2FP2 transition account
	TIM3->SMCR = TIM_SMCR_SMS_0 | TIM_SMCR_SMS_1;
	
	TIM3->CNT = 0x8000;																// Score up / down from the middle
	TIM3->ARR = 0x8000;																// Reloadable value
	
	TIM3->DIER = TIM_DIER_UIE;														// Enable Timer Interrupts
	
	TIM3->CR1 = TIM_CR1_CEN;														// Start timer
	
	return 0;
}

void TIM3_IRQHandler(void){
	TIM3->SR &= ~TIM_SR_UIF; 														// Clear falg UIF

}

//****************************************************************
//************************* Timer �4 ****************************
//****************************************************************
// ������� ��������� 1
int	Tim4Init(){
	RCC->APB1ENR |= RCC_APB1ENR_TIM4EN;												// Enable 4st timer clocking
	
	// Configure the 2nd multiplexer for the 2nd input ( IC2 = TI2 )
	// Configuring the 1st multiplexer for the 1st input ( IC1 = TI1 )
	// without filter and prescaler
	TIM4->CCMR1 = TIM_CCMR1_CC2S_0 | TIM_CCMR1_CC1S_0;
	
	// Capture on falling down imp. (CC1P=CC2P=1)
	// no counter capture in register (CC1E=CC2E=0)
	TIM4->CCER = TIM_CCER_CC1P | TIM_CCER_CC2P;
		
	// On Encoder interface: TI1FP1 transition and TI2FP2 transition account
	TIM4->SMCR = TIM_SMCR_SMS_0 | TIM_SMCR_SMS_1;
	
	TIM4->CNT = 0x8000;																// Score up / down from the middle
	TIM4->ARR = 0x8000;																// Reloadable value
	
	TIM4->DIER = TIM_DIER_UIE;														// Enable Timer Interrupts
	EnableIRQ( TIM4_IRQn );
	
	TIM4->CR1 = TIM_CR1_CEN;														// Start timer
		
	return 0;
}

void TIM4_IRQHandler(void){
	TIM4->SR &= ~TIM_SR_UIF; 														// Clear falg UIF
	
	// Turn off the engines
	Drv1BrOn();
	Drv1Dis();
	
	SysMode = SYS_SEEK1OK;															// Complete move mode
}

//****************************************************************
//************************* Timer �5 ****************************
//****************************************************************
// ������� ��������� 2
int	Tim5Init(){
	RCC->APB1ENR |= RCC_APB1ENR_TIM5EN;												// Enable 5st timer clocking
	
	// Configure the 2nd multiplexer for the 2nd input ( IC2 = TI2 )
	// Configuring the 1st multiplexer for the 1st input ( IC1 = TI1 )
	// without filter and prescaler
	TIM5->CCMR1 = TIM_CCMR1_CC2S_0 | TIM_CCMR1_CC1S_0;
	
	// Capture on falling down imp. (CC1P=CC2P=1)
	// no counter capture in register (CC1E=CC2E=0)
	TIM5->CCER = TIM_CCER_CC1P | TIM_CCER_CC2P;
		
	// On Encoder interface: TI1FP1 transition and TI2FP2 transition account
	TIM5->SMCR = TIM_SMCR_SMS_0 | TIM_SMCR_SMS_1;
	
	TIM5->CNT = 0x8000;																// Score up / down from the middle
	TIM5->ARR = 0x8000;																// Reloadable value
	
	TIM5->DIER = TIM_DIER_UIE;														// Enable Timer Interrupts
	EnableIRQ( TIM5_IRQn );
	
	TIM5->CR1 = TIM_CR1_CEN;														// Start timer
		
	return 0;
}

void TIM5_IRQHandler(void){
	TIM5->SR &= ~TIM_SR_UIF;														// Clear falg UIF
	
	// Turn off the engines
	Drv2BrOn();
	Drv2Dis();
	
	SysMode = SYS_SEEK2OK;															// Complete move mode	
}

