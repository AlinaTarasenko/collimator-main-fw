#include "inc\stm32f10x.h"
#include "inc\usart.h"
#include "inc\sysinit.h"
#include "inc\port_pin.h"

//************************ Data Structures USART3 ***************************
volatile	BYTE	Comm3Mode = U_WAIT;											// Port mode
volatile	BYTE	Comm3Err = 0;												// Receive Data Error Flag

#define	MAXLENPACK 			257													// Max. packet length 0xC0 + 257���� + 0xC0

#define	RX3_BUFFER_SIZE		512													// Input buffer size
volatile	WORD	Comm3BuffRx[RX3_BUFFER_SIZE];								// Receive buffer
volatile	WORD	Comm3BuffRxPntRd = 0;										// Pointer to read byte
volatile	WORD	Comm3BuffRxPntWr = 0;										// Pointer to write byte
volatile	WORD	Comm3BuffRxCnt = 0;											// Unread bytes in buffer

#define	TX3_BUFFER_SIZE		MAXLENPACK											// Output Buffer Size
volatile	BYTE	Comm3BuffTx[TX3_BUFFER_SIZE];								// Transfer buffer
volatile	BYTE	Comm3BuffTxIdxSend = 0;										// Pointer to the last byte transmitted
volatile	BYTE	Comm3BuffTxCnt = 0;											// Number of bytes in the write buffer
volatile	BYTE	Exch = 0;													// Byte code replacement when transmitting

volatile	WORD	Comm3CtPacket = 0;											// Packet Counter

//****************************************************************************
//************************** Setup USART3   ******************************
//****************************************************************************
int	USART3Init(void){
	// Transmission speed 115.2 kbps. USARTDIV=FSYS/(16*baud) = 36e6/1/(16*115200)=19.53125
		
	RCC->APB1ENR|= RCC_APB1ENR_USART3EN;															// Enable clocking USART3. 
	
	USART3->BRR = ( 19 << 4 );																		// Integer part of the division ratio USART3. 
	USART3->BRR |= 9;																				// Fractional*16 = 0,53125*16 = 9 (rounding off).
	USART3->CR1 |= ( USART_CR1_RE | USART_CR1_TE );													// Enable output RX, TX.
	
	USART3->SR &= ~USART_SR_TC;         															// Clear flag
	
	USART3->CR1 |= USART_CR1_TCIE;																	// Enable interrupt transfer
	USART3->CR1 |= USART_CR1_RXNEIE;																// Enable interrupt receive

	USART3->CR1 |= USART_CR1_UE;																	// Enable USART3.

	EnableIRQ( USART3_IRQn );																		// Enable interrupt USART3_IRQn in NVIC.
	
	return 0;
}

// ********************* Interrupt handler USART1 ***********************
void	USART3_IRQHandler( void ){
	BYTE	buff232;
	WORD	status;
	
	// If the interrupt on reception +++++++++++++++++++++++++++++++++++++
	if( USART3->SR & USART_SR_RXNE ){
		//status = USART2->ISR;																		// Get Status
		buff232 = USART3->DR;																		// Get accepted byte
		
		// !!!!!!!!! INDICATION RECEPTION TEAMS QUICK MERCY !!!!!!!!!!
		LedSysEx();
		
		// Standby mode
		if( ( Comm3Mode == U_WAIT )&&( buff232 == STX ) ){											// �If the packet is in standby mode STX - starting byte
			Comm3Mode = U_START;																	// - switch to the start mode of receiving a packet
		}
			
		// Start mode
		if( ( Comm3Mode == U_START )&&( buff232 != STX ) ){											// If in start mode another one  STX is accepted - ignore
			status = CommRdBuffWr( STX );															// - save start byte to buffer
			
			if( status ){																			// if the buffer is full
				Comm3Err = U_ERROR;																	// !!! BUFFER OVERFLOW ERROR !!!
				Comm3BuffRxPntWr = 0;																// Clear buffer
				Comm3BuffRxCnt = 0;																	// Reset the received bytes counter
				Comm3Mode = U_WAIT;																	// Return to package waiting
				return;
			}
			
			Comm3Mode = U_RPACK;																	// - switch to packet reception mode
		}

		// Packet reception mode		
		if( Comm3Mode == U_RPACK ){
			status = CommRdBuffWr( buff232 );														// save received data to buffer
			
			if( status ){																			// if the buffer is full
				Comm3Err = U_ERROR;																	// !!! BUFFER OVERFLOW ERROR !!!
				Comm3BuffRxPntWr = 0;																// Clear buffer
				Comm3BuffRxCnt = 0;																	// Reset the received bytes counter
				Comm3Mode = U_WAIT;																	// Return to package waiting
				return;
			}
			
			if( buff232 == STX ){																	// If in packet receive mode - STX = ETX (end of byte)
				Comm3CtPacket++;																	// - increase the count of received packets
				Comm3Mode = U_WAIT;																	// - return to waiting for the next batch
			}
		}
	}
	// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	
	// Interruption upon completion of the transfer ++++++++++++++++++++++++++++++
	if( USART3->SR & USART_SR_TC ){
		USART3->SR &= ~USART_SR_TC;         														// Clear falg
		
		if( Exch ){																					// If there is a code replacement byte
			USART3->DR = Exch;																		// - send byte
			Exch = 0;																				// - claer code replacement byte
		}else
		if( Comm3BuffTxCnt ){																		// If there is data to transmit
			
				// If 0xC and not the end of the packet - code replacement
				if( ( Comm3BuffTx[Comm3BuffTxIdxSend] == 0xC0 )&&( Comm3BuffTxCnt > 1 ) ){
					USART3->DR = 0xDB;																// - send 0xDB
					Exch = 0xDC;																	// - prepare the next byte 0xDC
				}else
				if( Comm3BuffTx[Comm3BuffTxIdxSend] == 0xDB ){										// If byte 0xDB
					USART3->DR = 0xDB;																// - send 0xDB
					Exch = 0xDD;																	// - prepare the next byte 0xDD
				}else{
					USART3->DR = Comm3BuffTx[Comm3BuffTxIdxSend];									// - send data
				}
				Comm3BuffTxIdxSend++;
				--Comm3BuffTxCnt;																	// - reduce counter
		}
	}
	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

}
// ****************************************************************************************

// ************************************************************
// ************* Writing data to a circular buffer ************
// ************************************************************
int	CommRdBuffWr( BYTE	bt ){
	
	if( Comm3BuffRxCnt == RX3_BUFFER_SIZE )
		return BUFFFULL;
	
	__disable_irq();																				// Disable interrupts
	Comm3BuffRx[Comm3BuffRxPntWr++] = bt;															// Buffer send data
	Comm3BuffRxCnt++;																				// Increase buffer data counter
	__enable_irq();																					// Enable interrupts
		
	if( Comm3BuffRxPntWr == RX3_BUFFER_SIZE )														// If the pointer is out of the buffer - return
		Comm3BuffRxPntWr = 0;
	
	return 0;
}

// ************************************************************
// ********** Reading data from a circular buffer ************
// ************************************************************
int	CommRdBuffRd( void ){
	BYTE	bt;
	
	if( Comm3BuffRxCnt == 0 )
		return BUFFEMPTY;
	
	__disable_irq();																				// Disable interrupts
	bt = Comm3BuffRx[Comm3BuffRxPntRd++];															// Read data from the buffer, increase the pointer
	Comm3BuffRxCnt--;																				// Reduce data buffer buffer
	__enable_irq();																					// Enable interrupts
	
	if( Comm3BuffRxPntRd == RX3_BUFFER_SIZE )														// If the pointer is out of the buffer - return
		Comm3BuffRxPntWr = 0;
	
	return bt;
}

// *********************************************************************
// **** Quickly copy a packet from the port buffer to the external buffer ****
// *********************************************************************
int	CommBuff2Buff( BYTE* buffIn, int len ){


	return 0;
}

